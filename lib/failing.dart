import 'package:flutter/material.dart';
import 'package:form_builder/menu.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';


class FailingPage extends StatefulWidget {
  @override
  _FailingPageState createState() => new _FailingPageState();
}

class _FailingPageState extends State<FailingPage> {
  String _aStringValue = '';

  @override
  void initState() {
    super.initState();
    _loadVals();
  }

  //Loading counter value on start
  _loadVals() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _aStringValue = prefs.getString('aStringValue');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SharedScaffold(
          titulo: 'Failing',
          body: Padding(
            padding: EdgeInsets.all(20.0),
            child: Padding(
            padding: EdgeInsets.all(20.0),
            child: FormBuilder(
                context,
                autovalidate: true,
                controls: [
                  FormBuilderInput.textField(
                    type: FormBuilderInput.TYPE_TEXT,
                    attribute: "aStringValue",
                    value: _aStringValue,
                    label: "Any Value:",
                  )

                ],
                onChanged: () async {

                },
                onSubmit: (formValue) async {
                  if (formValue != null) {
                    print(formValue);
                        final prefs = await SharedPreferences.getInstance();              
                        prefs.setString('aStringValue', formValue['aStringValue']);
                  } else {
                    print("Form invalid");
                  }
                },
              ),
          )
          )
      );
   
  }
}
