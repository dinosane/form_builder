import 'package:flutter/material.dart';
import 'package:form_builder/menu.dart';
import 'package:shared_preferences/shared_preferences.dart';


class WorkingPage extends StatefulWidget {
  @override
  _WorkingPageState createState() => new _WorkingPageState();
}

class _WorkingPageState extends State<WorkingPage> {
  final myController = TextEditingController();
  String _aStringValue = '';

  @override
  void initState() {
    super.initState();
    _loadVals();
  }

  //Loading counter value on start
  _loadVals() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      myController.text =  (prefs.getString('aStringValue') ?? '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SharedScaffold(
          titulo: 'Working',
          body: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Any Field Label'),
                TextFormField(
                  controller: myController,
                ),
                RaisedButton(
                  onPressed: () async {
                        final prefs = await SharedPreferences.getInstance();              
                        prefs.setString('aStringValue', myController.text);
                  },
                )

              ],
            )
            
          )
      );
   
  }
}
