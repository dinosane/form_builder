import 'package:flutter/material.dart';

class SharedScaffold extends StatelessWidget {
  final Widget body;
  final String titulo;

  SharedScaffold({this.titulo, this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text(titulo),
        ),
        body: body,
        drawer: new Drawer(
          child: Container(
              color: Colors.lightBlue[100],
              child: new ListView(
                children: <Widget>[
                  new ListTile(
                    title: new Text('Working'),
                    onTap: () {
                      Navigator.pushNamed(context, '/working');
                    },
                  ),
                  new ListTile(
                    title: new Text('Failing'),
                    onTap: () {
                      Navigator.pushNamed(context, '/failing');
                    },
                  ),
                ],
              )),
        ));
  }
}
