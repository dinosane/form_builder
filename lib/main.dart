import 'package:flutter/material.dart';
import 'package:form_builder/failing.dart';
import 'package:form_builder/working.dart';

void main() => runApp(new MyApp());

final routes = {
  "/working"   : (BuildContext context) => WorkingPage(),
  "/failing"    : (BuildContext context) => FailingPage(),
  };
  

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'FormBuiler Issue',
      theme: new ThemeData(primarySwatch: Colors.blue),
      home: WorkingPage(),
      routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}
